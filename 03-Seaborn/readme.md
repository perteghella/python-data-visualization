# Seaborn

Seaborn is a Python data visualization library based on *matplotlib*. It provides a high-level interface for drawing attractive and informative statistical graphics.
http://seaborn.pydata.org/

Data repository for seaborn examples https://github.com/mwaskom/seaborn-data

Start with  
- tips 
- iris
- official tutorial http://seaborn.pydata.org/tutorial.html#tutorial
- Example-gallery http://seaborn.pydata.org/examples/index.html#example-gallery

Final exercise : *Titanic*
- Jupiter notebook https://mybinder.org/v2/gl/perteghella%2Fpython-data-visualization/master?filepath=03-Seaborn%2Ftitanic_seaborn.ipynb

More sorurce od dataset and analysis https://www.kaggle.com/